<?php


function makeFinalJson($clave_xml,$xml_respuesta_hacienda)
{
    $clave                      =  $clave_xml; //params_get("clave");
    $fecha                      = params_get("fecha_emision");
    $emi_tipoIdentificacion     = params_get("emisor_tipo_identif");
    $emi_numeroIdentificacion   = params_get("emisor_num_identif");
    $recp_tipoIdentificacion    = params_get("receptor_tipo_identif");
    $recp_numeroIdentificacion  = params_get("receptor_num_identif");
    $comprobanteXml             = $xml_respuesta_hacienda; //params_get("comprobanteXml");
    $response                   = array(
                                    'clave'         => $clave,
                                    'fecha'         => $fecha,
                                    'emisor'        => array('tipoIdentificacion' => $emi_tipoIdentificacion, 'numeroIdentificacion' => $emi_numeroIdentificacion),
                                    'receptor'      => array('tipoIdentificacion' => $recp_tipoIdentificacion, 'numeroIdentificacion' => $recp_numeroIdentificacion),
                                    'comprobanteXml'=> $comprobanteXml);
    return  $response;
}
?>
