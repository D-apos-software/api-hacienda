<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

tools_useTool('MAIL/PHPMailer/src/PHPMailer.php');
tools_useTool('fpdf/fpdf.php');
tools_useTool('MAIL/PHPMailer/src/SMTP.php');
tools_useTool('MAIL/PHPMailer/vendor/autoload.php');
tools_useTool('MAIL/PHPMailer/src/Exception.php');
require 'ubicacionCR.php';

function sendmail($xml, $xml_firmado, $clave_f, $num_fact)
{
    //-------------------------------------------------------------------EXTRACTED DATA----------------------------------------------------------------------
    //--------------------- Emisor ---------------//
    $mail_emisor = params_get("emisor_email");
    $emisor_cod_pais_tel = params_get("emisor_cod_pais_tel");
    $emisor_tel = params_get("emisor_tel");
    $emisor_nombre = params_get("emisor_nombre");
    $emisor_num_identif = params_get("emisor_num_identif");
    $tipo_cambio = params_get("tipo_cambio");
    $fecha_emision = params_get("fecha_emision");
    $fecha_emision2 = substr($fecha_emision, 0, 10);
    $emisor_tipo_identif = params_get("emisor_tipo_identif");
    $password_email = params_get("password_email");



    $tipo_id = '';
    //--------------------- Validacion de tipo de identificacion -----------------//


    switch ($emisor_tipo_identif) {
        case '01': // Factura Electronica
            $tipo_id = "Fisica";
            break;
        case '02': // Nota de Debito
            $tipo_id = "Juridica";
            break;
    }

    //---------------------------------------------------------------------------//

    //--------------------- Cliente ---------------//
    $receptor_num_identif = params_get("receptor_num_identif");
    $receptor_nombre = params_get("receptor_nombre");
    $receptor_email = params_get("receptor_email");
    $fecha_emision = params_get("fecha_emision");
    $receptor_cod_pais_tel = params_get("receptor_cod_pais_tel");
    $receptor_tel = params_get("receptor_tel");

    //--------------------- Ubicacion -------------//
    $receptor_provincia = params_get("receptor_provincia");
    $receptor_canton = params_get("receptor_canton");
    $receptor_distrito = params_get("receptor_distrito");
    $receptor_barrio = params_get("receptor_barrio");

    $UbicacionCR = UbicacionCR($receptor_provincia, $receptor_canton, $receptor_distrito);

    //--------------------- Factura  -------------//
    $numero_factura = //params_get("numero_factura");
    $productos = params_get("detalles");
    $total_comprobante = params_get("total_comprobante");
    //-------------------------------------------------------------------EXTRACTED DATA----------------------------------------------------------------------


    $mail = new PHPMailer(TRUE);

    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   =  $mail_emisor;/*'usuariodapos@gmail.com';*/                     //SMTP username
    $mail->Password   =  $password_email; /* 'D-apos2021**'; */                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;

    $clave = $clave_f; //params_get("clave");


    $pdf = new FPDF();

    //-------------------------------------------------------------------HEADER----------------------------------------------------------------------
    //Logo
    //Pagina #1
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Image('https://www.d-apos.com/wp-content/uploads/2020/03/1500x500-PNG.png', 10, 8, 50, 18, 'PNG');
    //Datos de la empresa
    $pdf->Cell(60, 5, ' ', 0, 0);
    $pdf->Cell(80, 5, $emisor_nombre/*'D-apos '*/, 0, 0);
    $pdf->Cell(29, 5, 'Factura Electronica', 0, 1); //end of line
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(60, 5, ' ', 0, 0);
    $pdf->Cell(80, 5, 'Telefono:' . $emisor_cod_pais_tel . $emisor_tel, 0, 0);
    $pdf->Cell(29, 5, 'No: ' . $num_fact, 0, 1); //end of line
    $pdf->Cell(60, 5, ' ', 0, 0);
    $pdf->Cell(80, 5, 'Cedula ' . $tipo_id . ' : ' . $emisor_num_identif, 0, 0);
    $pdf->Cell(29, 5, 'Cambio Colon: ' . $tipo_cambio, 0, 1); //end of line
    $pdf->Cell(60, 5, ' ', 0, 0);
    $pdf->Cell(80, 5, 'Correo Electronico : ' . $mail_emisor, 0, 0);
    $pdf->Cell(29, 5, 'Fecha de Emision: ' . $fecha_emision2, 0, 1); //end of line
    $pdf->SetFont('Arial', '', 11);
    $pdf->Line(0, 35, 1000, 35);
    $pdf->SetDrawColor(151, 150, 142);
    //-------------------------------------------------------------------HEADER----------------------------------------------------------------------
    //-------------------------------------------------------------------BODY------------------------------------------------------------------------
    //Cell(width , height , text , border , end line , [align] )
    //Section 1

    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Cell(19, 18, ' ', 0, 1);
    $pdf->Cell(70, 5, 'Cliente', 0, 0);
    $pdf->Cell(0, 5, 'Clave de Factura', 0, 1); //end of line

    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(70, 5, $receptor_nombre /*'Gabriel Quesada Araya'*/, 0, 0); //end of line
    $pdf->Cell(59, 5, $clave, 0, 1); //end of line

    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Cell(70, 5, 'Direccion', 0, 0);
    $pdf->Cell(40, 5, 'Telefono', 0, 0);
    $pdf->Cell(70, 5, 'Identificacion', 0, 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(70, 5, $UbicacionCR, 0, 0);
    $pdf->Cell(40, 5, $receptor_cod_pais_tel . $receptor_tel, 0, 0); //end of line
    $pdf->Cell(10, 5, $receptor_num_identif, 0, 1); //end of line

    //--------------------------------------- Tabla ------------------------------------//
    //Section 2
    //make a dummy empty cell as a vertical spacer
    $pdf->Cell(189, 10, '', 0, 1); //end of line
    //invoice contents
    $pdf->SetFont('Arial', 'B', 11);

    $pdf->Cell(35, 5, 'Codigo', 1, 0);
    $pdf->Cell(27, 5, 'Cantidad', 1, 0);
    $pdf->Cell(48, 5, 'Description', 1, 0);
    //$pdf->Cell(20, 5, 'Unidad', 1, 0);
    $pdf->Cell(25, 5, 'Descuento', 1, 0);
    $pdf->Cell(25, 5, 'Impuesto', 1, 0);
    $pdf->Cell(30, 5, 'SubTotal', 1, 1); //end of line

    $pdf->SetFont('Arial', '', 11);
    //-------------------------------------------------------------------Productos------------------------------------------------------------------------
    //return $productosD[0] = (array) $detallesProductos;
    $manage = json_decode($productos, true);

    // $manage['1']['codigo'];
$SUBTOTAL = 0;
foreach($manage as $vals){
    $pdf->Cell(35, 5, $vals['codigo'], 1, 0);
    $pdf->Cell(27, 5, $vals['cantidad'], 1, 0);
    $pdf->Cell(48, 5, $vals['detalle'], 1, 0);
    $pdf->Cell(25, 5, '0', 1, 0);
    $pdf->Cell(25, 5, '0', 1, 0);
    $pdf->Cell(30, 5, $vals['subtotal'], 1, 1); //end of line
    $sub_pr = (int)$vals['subtotal'];
    $SUBTOTAL += $sub_pr;
}

    //-------------------------------------------------------------------Productos------------------------------------------------------------------------
    $pdf->Cell(59, 10, ' ', 0, 1);
    //$pdf->Line(28,95,180,95);
    //summary
    $pdf->Cell(121, 15, '', 0, 0);
    $pdf->Cell(35, 5, $SUBTOTAL, 0, 0);
    $pdf->Cell(4, 5, '$', 1, 0);
    $pdf->Cell(30, 5, '10', 1, 1, 'R'); //end of line

    $pdf->Cell(121, 5, '', 0, 0);
    $pdf->Cell(35, 5, 'Descuento', 0, 0);
    $pdf->Cell(4, 5, '$', 1, 0);
    $pdf->Cell(30, 5, '0%', 1, 1, 'R'); //end of line

    $pdf->Cell(121, 5, '', 0, 0);
    $pdf->Cell(35, 5, 'I.V.A', 0, 0);
    $pdf->Cell(4, 5, '$', 1, 0);
    $pdf->Cell(30, 5, '0%', 1, 1, 'R'); //end of line

    $pdf->Cell(121, 5, '', 0, 0);
    $pdf->Cell(35, 5, 'Otros Cargos', 0, 0);
    $pdf->Cell(4, 5, '$', 1, 0);
    $pdf->Cell(30, 5, '0%', 1, 1, 'R'); //end of line

    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(121, 5, '', 0, 0);
    $pdf->Cell(35, 5, 'Total:', 0, 0);
    $pdf->Cell(4, 5, '$', 1, 0);
    $pdf->Cell(30, 5,$total_comprobante, 1, 1, 'R'); //end of line

    //-------------------------------------------------------------------BODY------------------------------------------------------------------------
    //-------------------------------------------------------------------FOOTER------------------------------------------------------------------------
    $altura = ($pdf->GetPageHeight()) - 20;
    $alturaIMG = ($pdf->GetPageHeight()) - 100;
    $anchura = ($pdf->GetPageWidth());
    $pdf->SetY($altura);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Image('https://www.d-apos.com/wp-content/uploads/2022/01/d-APOS.png', 10, $alturaIMG, 190, 60, 'PNG');
    $pdf->text($anchura / 5, $altura, ' Emitida conforme lo establecido en la resolucion de Facturacion Electronica');
    $pdf->text($anchura / 5, $altura + 5, 'N DGT-R-48-2022 de enero de dos mil veinte de la Direccion General de Tributacion');
    //-------------------------------------------------------------------FOOTER------------------------------------------------------------------------
    $pdf->Output('F', 'file.pdf');


    //Recipients
    $mail->setFrom($mail_emisor/*'usuariodapos@gmail.com'*/, 'Mailer');
    $mail->addAddress($receptor_email/*'usuariodapos@gmail.com'*/, $receptor_nombre);     //Add a recipient
    //$mail->addAddress('usuariodapos@gmail.com');               //Name is optional
    $mail->addReplyTo($receptor_email, 'Information');
    $mail->addCC($receptor_email);
    $mail->addBCC($receptor_email);

    //Content
    $mail->isHTML(true);
    $mail->Subject = 'Factura Electronica con numero consecutivo ' . $num_fact . ' del ' . $fecha_emision2 . ' emitida por ' . $emisor_nombre . ' para ' . $receptor_nombre . ' ' . $clave;
    //$mail->Subject = '[br/] Documentos de Factura electronica #' . $clave;
    $mail->Body = 'Se adjuntan las facturas electronicas.';

    $url = 'file.pdf';
    $fichero = file_get_contents($url);
    $mail->addStringAttachment($fichero, 'solicitud.pdf');
    $mail->addStringAttachment(base64_decode($xml), 'Comprobante_' . $clave . '.xml');
    $mail->addStringAttachment(base64_decode($xml_firmado), 'MH_' . $clave . '.xml');
    //$mail->addStringAttachment(params_get("json"), 'MH_' . $clave . '.xml');

   if ($mail->send()) {
        return 'exitoso';
    } else {
        return 'No exitoso';
    } 

    //$temp = tmpfile();
    //$file_contents = $pdf->Output();

    /*$mail->addStringAttachment(base64_decode(params_get("xmlEnvia")), 'Comprobante_' . $clave . '.xml');
    $mail->addStringAttachment(base64_decode(params_get("xmlHacienda")), 'MH_' . $clave . '.xml');
    $mail->addStringAttachment($file_contents, 'name file.pdf');*/
}
