<?php


function UbicacionCR($Provincia, $Canton, $Distrito)
{
    $ubicacion_general = array();
    try {

        //Provincia
        $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincias.json");
        $Cantones = get_object_vars(json_decode($jsonc));
        array_push($ubicacion_general, $Cantones[$Provincia]);

        //Canton
        $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $Provincia . "/cantones.json");
        $Canton_general = get_object_vars(json_decode($jsonc));
        array_push($ubicacion_general, $Canton_general[$Canton]);

        //Distrito 
        $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $Provincia . "/canton/" . $Canton . "/distritos.json");
        $Distrito_general = get_object_vars(json_decode($jsonc));
        array_push($ubicacion_general, $Distrito_general[$Distrito]);
        $ubicacion_general = $ubicacion_general[0].' '.$ubicacion_general[1].' '.$ubicacion_general[2];
        return $ubicacion_general;

    } catch (Exception $e) {
        print("Error en Function Ubicacion :" . $e);
    }
}

