<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

tools_useTool('MAIL/PHPMailer/src/PHPMailer.php');
tools_useTool('fpdf/fpdf.php');
tools_useTool('MAIL/PHPMailer/src/SMTP.php');
tools_useTool('MAIL/PHPMailer/vendor/autoload.php');
tools_useTool('MAIL/PHPMailer/src/Exception.php');

function sendmail($xml, $xml_firmado,$clave_f,$num_fact)
{
    //-------------------------------------------------------------------EXTRACTED DATA----------------------------------------------------------------------
    //--------------------- Emisor ---------------//
    $mail_emisor = params_get("emisor_email");
    $emisor_cod_pais_tel = params_get("emisor_cod_pais_tel");
    $emisor_tel = params_get("emisor_tel");
    $emisor_nombre = params_get("emisor_nombre");
    $emisor_num_identif = params_get("emisor_num_identif");
    $tipo_cambio = params_get("tipo_cambio");
    $fecha_emision = params_get("fecha_emision");
    $fecha_emision2 = substr($fecha_emision, 0, 10);
    $emisor_tipo_identif = params_get("emisor_tipo_identif");
    $tipo_id = '';
    //--------------------- Validacion de tipo de identificacion -----------------//
    

        switch ($emisor_tipo_identif)
        {
            case '01': // Factura Electronica
                $tipo_id = "Fisica";
                break;
            case '02': // Nota de Debito
                $tipo_id = "Juridica";
                break;
        }
    
    //---------------------------------------------------------------------------//

    //--------------------- Cliente ---------------//
    $receptor_num_identif = params_get("receptor_num_identif");
    $receptor_nombre = params_get("receptor_nombre");
    $receptor_email = params_get("receptor_email");
    $fecha_emision = params_get("fecha_emision");
    $receptor_cod_pais_tel = params_get("receptor_cod_pais_tel");
    $receptor_tel = params_get("receptor_tel");

    //--------------------- Ubicacion -------------//
    $receptor_provincia = params_get("receptor_provincia");
    $receptor_canton = params_get("receptor_canton");
    $receptor_distrito = params_get("receptor_distrito");
    $receptor_barrio = params_get("receptor_barrio");

    //--------------------- Factura  -------------//
    $numero_factura = //params_get("numero_factura");

   
    //-------------------------------------------------------------------EXTRACTED DATA----------------------------------------------------------------------


    $mail = new PHPMailer(TRUE);

    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'usuariodapos@gmail.com';                     //SMTP username
    $mail->Password   = 'D-apos2021**';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;

    $clave = $clave_f; //params_get("clave");


    $pdf = new FPDF();

//-------------------------------------------------------------------HEADER----------------------------------------------------------------------
//Logo
//Pagina #1
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 11);
$pdf->Image('https://www.d-apos.com/wp-content/uploads/2020/03/1500x500-PNG.png', 10, 8, 50, 18, 'PNG');
//Datos de la empresa
$pdf->Cell(60, 5, ' ', 0, 0);
$pdf->Cell(80, 5, 'D-apos ', 0, 0); 
$pdf->Cell(29, 5, 'Factura Electronica', 0, 1); //end of line
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(60, 5, ' ', 0, 0);
$pdf->Cell(80, 5, 'Telefono: 61749563', 0, 0); 
$pdf->Cell(29, 5, 'No: 2001224588', 0, 1); //end of line
$pdf->Cell(60, 5, ' ', 0, 0);
$pdf->Cell(80, 5, 'Cedula fisica: 207910714 ', 0, 0); 
$pdf->Cell(29, 5, 'Cambio Colon: 567.57 ', 0, 1); //end of line
$pdf->Cell(60, 5, ' ', 0, 0); 
$pdf->Cell(80, 5, 'Correo Electronico : usuariosdapos@gmail.com ', 0, 0); 
$pdf->Cell(29, 5, 'Fecha de Emision: 27/2/2022 ', 0, 1); //end of line
$pdf->SetFont('Arial', '', 11);
$pdf->Line(0,35,1000,35);
$pdf->SetDrawColor(151, 150, 142);
//-------------------------------------------------------------------HEADER----------------------------------------------------------------------
//-------------------------------------------------------------------BODY------------------------------------------------------------------------
//Cell(width , height , text , border , end line , [align] )
//Section 1

$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(19, 18, ' ', 0, 1);
$pdf->Cell(70, 5, 'Cliente', 0, 0);
$pdf->Cell(0, 5, 'Clave de Factura', 0, 1); //end of line

$pdf->SetFont('Arial', '', 10);
$pdf->Cell(70, 5,'Gabriel Quesada Araya', 0, 0); //end of line
$pdf->Cell(59, 5, '2001145', 0, 1); //end of line
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(70, 5, 'Direccion', 0, 0);
$pdf->Cell(40, 5, 'Telefono', 0, 0);
$pdf->Cell(70, 5, 'Identificacion', 0, 1);

$pdf->SetFont('Arial', '', 10);
$pdf->Cell(70, 5, 'Alajuela, San Carlos, Ciudad Quesada', 0, 0);
$pdf->Cell(40, 5, '+5066174963', 0, 0); //end of line
$pdf->Cell(10, 5, '207910714', 0, 1); //end of line


//Section 2
//make a dummy empty cell as a vertical spacer
$pdf->Cell(189, 10, '', 0, 1); //end of line
//invoice contents
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(45, 5, 'Description', 1, 0);
$pdf->Cell(20, 5, 'Codigo', 1, 0);
$pdf->Cell(20, 5, 'Unidad', 1, 0);
$pdf->Cell(25, 5, 'Descuento', 1, 0);
$pdf->Cell(20, 5, 'Impuesto', 1, 0);
$pdf->Cell(30, 5, 'Pre. Unidad', 1, 0);
$pdf->Cell(30, 5, 'SubTotal', 1, 1); //end of line

$pdf->SetFont('Arial', '', 11);
//-------------------------------------------------------------------Productos------------------------------------------------------------------------
$pdf->Cell(45, 5, 'Impresora', 1, 0);
$pdf->Cell(20, 5, '123', 1, 0);
$pdf->Cell(20, 5, 'Sp', 1, 0);
$pdf->Cell(25, 5, '0', 1, 0);
$pdf->Cell(20, 5, '0', 1, 0);
$pdf->Cell(30, 5, '10', 1, 0);
$pdf->Cell(30, 5, '10', 1, 1); //end of line
$pdf->Cell(45, 5, 'Impresora', 1, 0);
$pdf->Cell(20, 5, '123', 1, 0);
$pdf->Cell(20, 5, 'Sp', 1, 0);
$pdf->Cell(25, 5, '0', 1, 0);
$pdf->Cell(20, 5, '0', 1, 0);
$pdf->Cell(30, 5, '10', 1, 0);
$pdf->Cell(30, 5, '10', 1, 1); //end of line
$pdf->Cell(45, 5, 'Impresora', 1, 0);
$pdf->Cell(20, 5, '123', 1, 0);
$pdf->Cell(20, 5, 'Sp', 1, 0);
$pdf->Cell(25, 5, '0', 1, 0);
$pdf->Cell(20, 5, '0', 1, 0);
$pdf->Cell(30, 5, '10', 1, 0);
$pdf->Cell(30, 5, '10', 1, 1); //end of line

//-------------------------------------------------------------------Productos------------------------------------------------------------------------
$pdf->Cell(59, 10, ' ', 0, 1);
//$pdf->Line(28,95,180,95);
//summary
$pdf->Cell(121, 15, '', 0, 0);
$pdf->Cell(35, 5, 'Subtotal', 0, 0);
$pdf->Cell(4, 5, '$', 1, 0);
$pdf->Cell(30, 5, '10', 1, 1, 'R'); //end of line

$pdf->Cell(121, 5, '', 0, 0);
$pdf->Cell(35, 5, 'Descuento', 0, 0);
$pdf->Cell(4, 5, '$', 1, 0);
$pdf->Cell(30, 5, '0%', 1, 1, 'R'); //end of line

$pdf->Cell(121, 5, '', 0, 0);
$pdf->Cell(35, 5, 'I.V.A', 0, 0);
$pdf->Cell(4, 5, '$', 1, 0);
$pdf->Cell(30, 5, '0%', 1, 1, 'R'); //end of line

$pdf->Cell(121, 5, '', 0, 0);
$pdf->Cell(35, 5, 'Otros Cargos', 0, 0);
$pdf->Cell(4, 5, '$', 1, 0);
$pdf->Cell(30, 5, '0%', 1, 1, 'R'); //end of line


$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(121, 5, '', 0, 0);
$pdf->Cell(35, 5, 'Total:', 0, 0);
$pdf->Cell(4, 5, '$', 1, 0);
$pdf->Cell(30, 5, '10', 1, 1, 'R'); //end of line
//-------------------------------------------------------------------BODY------------------------------------------------------------------------
//-------------------------------------------------------------------FOOTER------------------------------------------------------------------------
$altura = ($pdf->GetPageHeight()) - 20;
$alturaIMG = ($pdf->GetPageHeight()) - 100;
$anchura = ($pdf->GetPageWidth());
$pdf->SetY($altura);
$pdf->SetFont('Arial', '', 10);
$pdf->Image('https://scontent.fsjo8-1.fna.fbcdn.net/v/t39.30808-6/272000172_5539705476056420_8003450677345503790_n.png?_nc_cat=106&ccb=1-5&_nc_sid=e3f864&_nc_ohc=jOiuE9OkPFUAX_zMUHb&_nc_oc=AQkHelGnWomP6e6viUtPfLopbbADkuxa1PHyyGadkcoXoFd_aFFkSRzue4LErJ8IjIc&_nc_ht=scontent.fsjo8-1.fna&oh=00_AT96W2DG9_qhyXq-0DhTabG9HcVxr6o2rf7M1llEWFp34g&oe=61F48AB4', 10, $alturaIMG, 190,60, 'PNG');
$pdf->text($anchura / 5, $altura, ' Emitida conforme lo establecido en la resolucion de Facturacion Electronica');
$pdf->text($anchura / 5, $altura + 5, 'N DGT-R-48-2022 de enero de dos mil veinte de la Direccion General de Tributacion');
//-------------------------------------------------------------------FOOTER------------------------------------------------------------------------
$pdf->Output('F','file.pdf');


    //Recipients
    $mail->setFrom('usuariodapos@gmail.com', 'Mailer');
    $mail->addAddress('usuariodapos@gmail.com', 'Gabriel Quesada');     //Add a recipient
    //$mail->addAddress('usuariodapos@gmail.com');               //Name is optional
    $mail->addReplyTo('usuariodapos@gmail.com', 'Information');
    $mail->addCC('usuariodapos@gmail.com');
    $mail->addBCC('usuariodapos@gmail.com');

    //Content
    $mail->isHTML(true);
    $mail->Subject = 'Documentos de Factura electronica #' . $clave;
    $mail->Body = 'Se adjuntan las facturas electronicas.';

    $url = 'file.pdf';
    $fichero = file_get_contents($url);
    $mail->addStringAttachment($fichero, 'solicitud.pdf');
    $mail->addStringAttachment(base64_decode($xml), 'Comprobante_' . $clave . '.xml');
    $mail->addStringAttachment(base64_decode($xml_firmado), 'MH_' . $clave . '.xml');
    //$mail->addStringAttachment(params_get("json"), 'MH_' . $clave . '.xml');

    if($mail->send()){
        return 'exitoso';
    }
    else{
        return 'No exitoso';

    } 







    //$temp = tmpfile();
    //$file_contents = $pdf->Output();

    /*$mail->addStringAttachment(base64_decode(params_get("xmlEnvia")), 'Comprobante_' . $clave . '.xml');
    $mail->addStringAttachment(base64_decode(params_get("xmlHacienda")), 'MH_' . $clave . '.xml');
    $mail->addStringAttachment($file_contents, 'name file.pdf');*/


   
}


