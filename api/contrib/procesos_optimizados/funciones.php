<?php
//Importar procesos a realizar
require 'Token/token.php';
require 'Autenticacion/solicitud_token.php';
require 'ClaveXML/clave.php';
require 'CrearXml/xml.php';
require 'FirmaXml/firmarXml.php';
require 'Enviar_XML_Hacienda/envio_hacienda.php';
require 'CrearJson/Json.php';
require 'Estado/respuesta.php';
require 'Enviar_documentos/documentos.php';

//Proceso #1 Subir llave criptografica 
function llave_criptografica()
{
    $llave = uploadCert();
    return $llave['downloadCode'];
}
//Proceso #2 Autenticacion en Hacienda
function Autenticacion_hacienda()
{
    $token = token();
    return $token->access_token;
}

//Proceso #3 Creacion de Clave XML 
function Clave_XML()
{
    return $ClaveXML = getClave();
    $clave =  $ClaveXML['clave'];
    $consecutivo = $ClaveXML['consecutivo'];
}
//Proceso #4 Creacion de XML 
function Creacion_XML($clave, $consecutivo)
{
    $Xml = genXMLFe($clave, $consecutivo);
    $XmlClave = $Xml['clave'];
    $XmlB64 = $Xml['xml'];
}
//Proceso #5 Firmar XML
function Firmar_xml($Token_ctr, $XmlB64)
{
    return $Xml_firmado = signFE($Token_ctr, $XmlB64);
    $R_XmlFirmado =  $Xml_firmado['xmlFirmado'];
}
//Proceso #6 Enviar Hacienda 
function  Enviar_Hacienda($Token_session, $XmlClave, $R_XmlFirmado)
{
    return $Envio_hacienda = send($Token_session, $XmlClave, $R_XmlFirmado);
}
//Proceso #7 Ver Estado 
function Estado($XmlClave, $Token_session)
{

    sleep(3);
    return $Cons_r = consutar($XmlClave, $Token_session);
    $Cons_r->clave;
    //Convertimos en array
    $thearray = (array) $Cons_r;
    $xml_firmado = $thearray['respuesta-xml'];
    $cont = 0;
}
//Proceso #8 ENVIAR DOCUMENTOS 
function Enviar_Documentos($R_XmlFirmado, $xml_firmado)
{
    return $envio_docs = sendmail($R_XmlFirmado, $xml_firmado);
}
