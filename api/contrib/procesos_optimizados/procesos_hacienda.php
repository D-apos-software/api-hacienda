<?php

//Importar procesos a realizar
require 'Token/token.php';
require 'Autenticacion/solicitud_token.php';
require 'ClaveXML/clave.php';
require 'CrearXml/xml.php';
require 'FirmaXml/firmarXml.php';
require 'Enviar_XML_Hacienda/envio_hacienda.php';
require 'CrearJson/Json.php';
require 'Estado/respuesta.php';
require 'Enviar_documentos/documentos.php';



function procesos()
{

    /*
        Pendiente si enviar o no archivo Json.

    */

    //Proceso #1 Subir llave criptografica 
    try {
        //$llave = uploadCert();
        $Token_ctr = params_get("token_llave"); // $llave['downloadCode'];
        //return $llave;
        if (isset($Token_ctr)) {
            //Proceso #2 Autenticacion en Hacienda
            try {
                $token = token();
                $Token_session = $token->access_token;
                if (isset($Token_session)) {
                    //Proceso #3 Creacion de Clave XML
                    try {
                        $ClaveXML = getClave();
                        $clave =  $ClaveXML['clave'];
                        $consecutivo =  $ClaveXML['consecutivo'];
                        $num_fact =  $ClaveXML['num_fact'];
                        if (isset($clave) && isset($consecutivo)) {

                            //Proceso #4 Creacion de XML

                            $Xml = genXMLFe($clave, $consecutivo);
                            $XmlClave = $Xml['clave'];
                            $XmlB64 = $Xml['xml'];
                            if (isset($XmlClave) && isset($XmlB64)) {

                                //Proceso #5 Firmar XML
                                $Xml_firmado = signFE($Token_ctr, $XmlB64);
                                $R_XmlFirmado =  $Xml_firmado['xmlFirmado'];
                                if (isset($R_XmlFirmado)) {

                                    //Proceso #6 Enviar Hacienda
                                    $Envio_hacienda = send($Token_session, $XmlClave, $R_XmlFirmado);

                                    if (isset($Envio_hacienda)) {

                                        //Proceso #7 Ver Estado 
                                        sleep(10);
                                        return $Cons_r = consutar($XmlClave, $Token_session);
                                        $Cons_r->clave;
                                        //Convertimos en array
                                        $thearray = (array) $Cons_r;
                                        $xml_firmado = $thearray['respuesta-xml'];
                                        if ($thearray['ind-estado'] == 'aceptado') {
                                            $envio_docs = sendmail($R_XmlFirmado, $xml_firmado, $XmlClave, $consecutivo);
                                            return 'exitoso';
                                            /*   return $envio_docs = sendmail(
                                                $R_XmlFirmado = "123",
                                                 $xml_firmado = "123",
                                                  $XmlClave = "123",
                                                   $consecutivo = "123"
                                                
                                                ); */

                                           /*  if ($envio_docs == "exitoso") {
                                                return "Procesos completado";
                                            } else {
                                                return "Procesos de envio incompleto";
                                            } */
                                        } else {
                                            return 'Estado no aceptado';
                                        }
                                    } else {
                                        return 'Error en Envio hacienda';
                                    }
                                } else {
                                    return 'Error en Firma XML';
                                }
                            } else {
                                return 'Fallo en la creacion de xml y clave';
                            }
                        } else {
                            return 'Fallo en la creacion de clave y consecutivo';
                        }
                    } catch (Exception $e) {
                        return 'Error en clave y consecutivo' . $e;
                    }
                } else {
                    return 'Fallo en el token de acceso';
                }
            } catch (Exception $e) {
                return 'No se logro ejecutar el proceso token de acceso';
            }
        } else {
            return 'Fallo en el token de la llave criptografica';
        }
    } catch (Exception $e) {
        return 'No se logro ejecutar el proceso de llave criptografica';
    }
}


//Proceso #1 llave
function toke_llave()
{

    $llave = uploadCert();
    $Token_ctr = $llave['downloadCode'];
    return $llave;
}


//Proceso #2 token de acceso
function token_acceso()
{
    $token = token();
    return $token->access_token;
}


//Proceso #3 y #4 Claves y XML
function creacion_xml_firma_clave()
{
    //Proceso #3 Creacion de Clave XML

    $ClaveXML = getClave();
    $clave =  $ClaveXML['clave'];
    $consecutivo =  $ClaveXML['consecutivo'];
    $num_fact =  $ClaveXML['num_fact'];


    //Proceso #4 Creacion de XML

    $Xml = genXMLFe($clave, $consecutivo);
    $XmlClave = $Xml['clave'];
    $XmlB64 = $Xml['xml'];


    //Proceso #5 Firmar XML

    $Xml_firmado = signFE(params_get('token_llave'), $XmlB64);
    $R_XmlFirmado =  $Xml_firmado['xmlFirmado'];
    return $list = json_encode([$R_XmlFirmado, $XmlClave,$consecutivo]);
}


function enviar_hacienda()
{
    $Envio_hacienda = send(params_get('Token_session'), params_get('XmlClave'), params_get('R_XmlFirmado'));
    return $Envio_hacienda;
}

function estado_hacienda()
{
    sleep(5);
    $Cons_r = consutar(params_get('XmlClave'),params_get('Token_session'));
    $Cons_r->clave;
    $Cons_r;
    //Convertimos en array
    $thearray = (array) $Cons_r;
    $xml_firmado = $thearray['respuesta-xml'];
    $estado = $thearray['ind-estado'];
    $list = [$xml_firmado,$estado];
    return json_encode($list); 
}

 
function enviar_doc()
{
    return sendmail(params_get('R_XmlFirmado'),params_get('xml_firmado'),params_get('XmlClave'),params_get('consecutivo'));
}
