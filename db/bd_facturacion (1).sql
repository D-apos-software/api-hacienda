-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-01-2022 a las 16:45:26
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_facturacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conversations`
--
-- Error leyendo la estructura de la tabla bd_facturacion.conversations: #1932 - Table 'bd_facturacion.conversations' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.conversations: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`conversations`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--
-- Error leyendo la estructura de la tabla bd_facturacion.documentos: #1932 - Table 'bd_facturacion.documentos' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.documentos: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`documentos`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emisores`
--

CREATE TABLE `emisores` (
  `idEmisor` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipoCedula` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razonSocial` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(15) NOT NULL,
  `fax` int(11) NOT NULL,
  `provincia` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `canton` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distrito` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barrio` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigoPais` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estatus` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emisoresactividades`
--
-- Error leyendo la estructura de la tabla bd_facturacion.emisoresactividades: #1932 - Table 'bd_facturacion.emisoresactividades' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.emisoresactividades: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`emisoresactividades`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files`
--
-- Error leyendo la estructura de la tabla bd_facturacion.files: #1932 - Table 'bd_facturacion.files' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.files: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`files`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lestatz_domains`
--

CREATE TABLE `lestatz_domains` (
  `idDomain` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `domain` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='A list of domains per user';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lestatz_goals`
--

CREATE TABLE `lestatz_goals` (
  `idGoal` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `goal` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `idDomain` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='A list of goals per user';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lestatz_refs`
--

CREATE TABLE `lestatz_refs` (
  `idRef` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `ref` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `idDomain` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='A list of refs per user';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lestatz_visit_log`
--
-- Error leyendo la estructura de la tabla bd_facturacion.lestatz_visit_log: #1932 - Table 'bd_facturacion.lestatz_visit_log' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.lestatz_visit_log: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`lestatz_visit_log`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marter_logs`
--

CREATE TABLE `marter_logs` (
  `idLog` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_certi`
--
-- Error leyendo la estructura de la tabla bd_facturacion.master_certi: #1932 - Table 'bd_facturacion.master_certi' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.master_certi: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`master_certi`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_claves`
--

CREATE TABLE `master_claves` (
  `idClave` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_companyusers`
--

CREATE TABLE `master_companyusers` (
  `idUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_comprobantes`
--

CREATE TABLE `master_comprobantes` (
  `idComprobante` int(11) NOT NULL,
  `idComprobanteReferencia` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `tipoDocumento` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `xmlEnviadoBase64` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `respuestaMHBase64` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT current_timestamp(),
  `idReceptor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Tabala de los comprobantes electronicos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_emisores`
--

CREATE TABLE `master_emisores` (
  `idUser` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` int(15) NOT NULL,
  `tipoCedula` int(10) NOT NULL,
  `razonSocial` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(15) NOT NULL,
  `provincia` int(11) NOT NULL,
  `canton` int(11) NOT NULL,
  `distrito` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_inventario`
--

CREATE TABLE `master_inventario` (
  `idProducto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_permission`
--

CREATE TABLE `master_permission` (
  `idCompanyUser` int(11) NOT NULL,
  `idRol` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_receptores`
--

CREATE TABLE `master_receptores` (
  `idReceptor` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` int(15) NOT NULL,
  `razonSocial` int(50) NOT NULL,
  `tipoCedula` int(10) NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_rol`
--

CREATE TABLE `master_rol` (
  `idRol` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master_sqlupgrade`
--

CREATE TABLE `master_sqlupgrade` (
  `idSQL` int(11) NOT NULL,
  `versionAPI` double NOT NULL,
  `SQL` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `msgs`
--
-- Error leyendo la estructura de la tabla bd_facturacion.msgs: #1932 - Table 'bd_facturacion.msgs' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.msgs: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`msgs`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receptores`
--

CREATE TABLE `receptores` (
  `idReceptor` int(11) NOT NULL,
  `idEmisor` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` int(15) NOT NULL,
  `tipoCedula` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `razonSocial` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(15) NOT NULL,
  `fax` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `canton` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distrito` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barrio` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigoPais` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--
-- Error leyendo la estructura de la tabla bd_facturacion.sessions: #1932 - Table 'bd_facturacion.sessions' doesn't exist in engine
-- Error leyendo datos de la tabla bd_facturacion.sessions: #1064 - Algo está equivocado en su sintax cerca 'FROM `bd_facturacion`.`sessions`' en la linea 1

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `idUser` int(11) UNSIGNED NOT NULL,
  `fullName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  `lastAccess` int(11) UNSIGNED NOT NULL,
  `pwd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Any and all settings you would like to set',
  `tipoConexion` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuarioHacienda` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwordHacienda` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`idUser`, `fullName`, `userName`, `email`, `about`, `country`, `status`, `timestamp`, `lastAccess`, `pwd`, `avatar`, `settings`, `tipoConexion`, `usuarioHacienda`, `passwordHacienda`) VALUES
(6, 'Gabriel Borbon', 'GabrielQA', 'Gabrielquesada1999@gmail.com', 'otro Usuaruio', 'CR', '1', 1642263549, 1643382208, 'TkROa01taFJNVXBVWm1SNU0yRlpNMmxwVURSc1oyazVMM0ZwTkRsTloydG5RbUl5UTI5VmJ6Tm9jMFl3VG0xa01DOVplVVFyUldjd2NtSkZZMlZRUlZWQ1oxVjVUa2RTVG10a1pFWm1ZMFpITVZOMFMxRTlQVG82ckN1ZW1vaGE2bmwwQWxLNmhlSTI3QT09', '0', 'NULL', '', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `emisores`
--
ALTER TABLE `emisores`
  ADD PRIMARY KEY (`idEmisor`);

--
-- Indices de la tabla `lestatz_domains`
--
ALTER TABLE `lestatz_domains`
  ADD PRIMARY KEY (`idDomain`),
  ADD UNIQUE KEY `idDomain` (`idDomain`);

--
-- Indices de la tabla `lestatz_goals`
--
ALTER TABLE `lestatz_goals`
  ADD PRIMARY KEY (`idGoal`),
  ADD UNIQUE KEY `idGoal` (`idGoal`);

--
-- Indices de la tabla `lestatz_refs`
--
ALTER TABLE `lestatz_refs`
  ADD PRIMARY KEY (`idRef`);

--
-- Indices de la tabla `marter_logs`
--
ALTER TABLE `marter_logs`
  ADD PRIMARY KEY (`idLog`);

--
-- Indices de la tabla `master_claves`
--
ALTER TABLE `master_claves`
  ADD PRIMARY KEY (`idClave`);

--
-- Indices de la tabla `master_companyusers`
--
ALTER TABLE `master_companyusers`
  ADD PRIMARY KEY (`idUser`);

--
-- Indices de la tabla `master_comprobantes`
--
ALTER TABLE `master_comprobantes`
  ADD PRIMARY KEY (`idComprobante`);

--
-- Indices de la tabla `master_inventario`
--
ALTER TABLE `master_inventario`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `master_receptores`
--
ALTER TABLE `master_receptores`
  ADD PRIMARY KEY (`idReceptor`);

--
-- Indices de la tabla `master_rol`
--
ALTER TABLE `master_rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `master_sqlupgrade`
--
ALTER TABLE `master_sqlupgrade`
  ADD PRIMARY KEY (`idSQL`);

--
-- Indices de la tabla `receptores`
--
ALTER TABLE `receptores`
  ADD PRIMARY KEY (`idReceptor`),
  ADD KEY `nombre` (`nombre`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `wire_2` (`userName`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `idUser` (`idUser`),
  ADD KEY `wire` (`userName`),
  ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `lestatz_domains`
--
ALTER TABLE `lestatz_domains`
  MODIFY `idDomain` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lestatz_goals`
--
ALTER TABLE `lestatz_goals`
  MODIFY `idGoal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lestatz_refs`
--
ALTER TABLE `lestatz_refs`
  MODIFY `idRef` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marter_logs`
--
ALTER TABLE `marter_logs`
  MODIFY `idLog` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `master_claves`
--
ALTER TABLE `master_claves`
  MODIFY `idClave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `master_companyusers`
--
ALTER TABLE `master_companyusers`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `master_comprobantes`
--
ALTER TABLE `master_comprobantes`
  MODIFY `idComprobante` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `master_inventario`
--
ALTER TABLE `master_inventario`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `master_receptores`
--
ALTER TABLE `master_receptores`
  MODIFY `idReceptor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `master_rol`
--
ALTER TABLE `master_rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `master_sqlupgrade`
--
ALTER TABLE `master_sqlupgrade`
  MODIFY `idSQL` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `receptores`
--
ALTER TABLE `receptores`
  MODIFY `idReceptor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
